const books = [
{ 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
}, 
{
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
}, 
{ 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
}, 
{ 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
}, 
{
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
},
{
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
}
];

function isValidBook(book) {
return 'author' in book && 'name' in book && 'price' in book;
}

function generateBooksList(booksArray) {
const rootElement = document.getElementById('root');
const ulElement = document.createElement('ul');

booksArray.forEach(book => {
    try {
    if (isValidBook(book)) {
        const liElement = document.createElement('li');
        liElement.textContent = `${book.author || 'Unknown Author'} - ${book.name || 'Unknown Name'} - ${book.price || 'Unknown Price'}`;
        ulElement.appendChild(liElement);
    } else {
        throw new Error(`Invalid book object: ${JSON.stringify(book)}`);
    }
    } catch (error) {
    console.error(error.message);
    }
});

rootElement.appendChild(ulElement);
}

generateBooksList(books);